package com.rgplay.memorygamek

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.rgplay.memorygamek.models.BoardSize
import com.rgplay.memorygamek.models.MemoryCard
import kotlin.math.min

class MemoryBoardAdapter(
    private val context: Context,
    private val memoryCardsSize: BoardSize,
    private val cardImages: List<MemoryCard>) : RecyclerView.Adapter<MemoryBoardAdapter.ViewHolder>() {

    companion object{
        private const val TAG = "MemoryBoardAdapter"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val cardWidth = parent.width/ memoryCardsSize.getWidth()
        val cardHeight = parent.height/ memoryCardsSize.getHeight()


        val cardSize = min(cardWidth, cardHeight)

        val view:View = LayoutInflater.from(context).inflate(R.layout.memory_card, parent, false)
        val layoutParams: ViewGroup.LayoutParams = view.findViewById<CardView>(R.id.cvMemoryCard).layoutParams
        layoutParams.width  = cardSize
        layoutParams.height = cardSize

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount() = memoryCardsSize.numCards

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        private val imageButton = itemView.findViewById<ImageButton>(R.id.ibCardElement)

        fun bind(position: Int) {
            val memoryCard: MemoryCard = cardImages[position]
            imageButton.setImageResource(if (memoryCard.isFaceUp) memoryCard.identifier else R.drawable.ic_launcher_background)
            imageButton.setOnClickListener{
                // on tap, toogle the image
                // check if any other card already
                Log.i(TAG,"clicked on $position")
            }
        }

    }

}
