package com.rgplay.memorygamek.utils

import com.rgplay.memorygamek.R


val DEFAULT_ICONS = listOf(
    R.drawable.ic_baseline_local_airport_24,
    R.drawable.ic_baseline_local_florist_24,
    R.drawable.ic_baseline_offline_pin_24,
    R.drawable.ic_baseline_rowing_24,
    R.drawable.ic_baseline_time_to_leave_24,
    R.drawable.ic_baseline_train_24,
    R.drawable.ic_baseline_two_wheeler_24,
    R.drawable.ic_baseline_watch_24,
    R.drawable.ic_baseline_wb_sunny_24,
    R.drawable.ic_baseline_wifi_24,
    R.drawable.ic_baseline_wine_bar_24,
    R.drawable.ic_outline_visibility_24
)