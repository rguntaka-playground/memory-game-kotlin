package com.rgplay.memorygamek.models

import com.rgplay.memorygamek.utils.DEFAULT_ICONS

class MemoryGame(private val boardSize: BoardSize) {
    val memoryCards: List<MemoryCard>
    val numPairsFound = 0

    init{
        var choosenImages: List<Int> = DEFAULT_ICONS.shuffled().take(boardSize.getNumPairs())
        var ranomizedImages: List<Int> = (choosenImages + choosenImages).shuffled()

        memoryCards = ranomizedImages.map { MemoryCard(it) }
    }
}